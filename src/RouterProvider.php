<?php


namespace Soen\Router;


use Soen\Filesystem\File;
use Soen\Http\Message\Request;
use Soen\Http\Message\Response;

class RouterProvider
{
    public $routeCurrent;
	function __construct($routesPath)
	{
	    $this->parse($routesPath);
	}

	function parse($routesPath){

        $files = (new File())->createFilesystemIterator($routesPath);
        $files->scanFiles(function ($filesystemIterator){
			    $filePath = $filesystemIterator->getPathname();
			    if($filesystemIterator->getExtension() === 'php') {
				    $array[] = require_once $filePath;
			    }
        });
	}

    /**
     * @param Request $request
     * @return RouteCurrent|null
     */
	function setRouteCurrent(Request $request){
		$this->routeCurrent = (Router::new())->getRouteCurrent($request);
		return $this->routeCurrent;
	}

	function getRouteCurrent(Request $request, Response $response){
		return $this->routeCurrent;
	}
}